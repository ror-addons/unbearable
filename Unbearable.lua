Unbearable = {}

function Unbearable.Initialize()
	LayoutEditor.RegisterWindow("UnbearableToggle",L"Unbearable Toggler",L"Unbearable Toggler",true,true,false,nil )
end

function Unbearable.Unset()
	for i, d in ipairs(GuildWindowTabRoster.memberListData) do
        if ( d.bearerStatus > 0 ) then
            if ( d.zoneID == 0 ) then
                SystemData.UserInput.ChatText = L"/GuildRemoveStandardBearer "..d.name
	            BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
            end
        end
    end
end

function Unbearable.Set()
    SystemData.UserInput.ChatText = L"/GuildAddStandardBearer "..GameData.Player.name
    BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
end