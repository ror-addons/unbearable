<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Unbearable" gameVersion="1.3.5" version="1.0" date="5/25/2010" >

		<Author name="Foghladha" email="bfoley@gaisciochnaanu.com" />
		<Description text="Left Click: Removes A Standard Bearer, Right Click: Sets You As A Standard Bearer" />

		<Dependencies>
	    	<Dependency name="EA_ChatWindow" />
        </Dependencies>

		<Files>
			<File name="Unbearable.xml" />
			<File name="Unbearable.lua" />
		</Files>

		<OnInitialize>
			<CreateWindow name="UnbearableToggle" show="true" />
			<CallFunction name="Unbearable.Initialize" />
		</OnInitialize>

	</UiMod>
</ModuleFile>
